import React from 'react';
import { BodySection, Header, ThemeContextProvider, PrintButton, RepoButton } from './components';
import { Awards, Contact, Career, SideProjects, Tools } from './components/content';

export const App = () => {
  const row = 'flex flex-col m-2';
  return (
    <ThemeContextProvider>
      <div className="font-sans font-semibold bg-paper text-primary container">
        <Header/>
        <div className="flex flex-col">
          <div className={`${row} flex-wrap sm:flex-row md:flex-row-reverse print:flex-row`}>
            <div className="flex w-full sm:w-1/2 lg:w-1/3 sm:pr-2 md:pr-0 md:pl-2 print:w-1/2 print:pl-0">
              <BodySection title="Contact" className="flex flex-col flex-1">
                <Contact/>
              </BodySection>
            </div>
            <div className="flex w-full sm:w-1/2 lg:w-1/3 mt-4 sm:mt-0 lg:pl-2 sm:pl-2 md:pl-0 md:pr-2 print:w-1/2 print:mt-0 print:pr-0">
              <BodySection title="Primary Tools" className="flex flex-col flex-1">
                <Tools/>
              </BodySection>
            </div>
            <div className="flex w-full lg:w-1/3 mt-4 lg:mt-0 lg:pr-2 print:mt-4 print:pr-0">
              <BodySection title="Recent Awards" className="flex flex-col flex-1">
                <Awards/>
              </BodySection>
            </div>
          </div>
          <div className={`${row} md:flex-col`}>
            <div className="w-full pr-0 md:pr-2 print:pr-2">
              <BodySection title="Career">
                <Career/>
              </BodySection>
            </div>
            <div className="w-full pl-0 mt-4 md:pl-2 md:mt-0 print:pl-2 print:mt-0">
              <BodySection title="Side Projects">
                <SideProjects/>
              </BodySection>
            </div>
          </div>
        </div>
      </div>
      <div className="top-0 right-0 absolute pt-3 pr-3">
        <div className="pb-3">
          <PrintButton />
        </div>
        <div>
          <RepoButton />
        </div>
      </div>
    </ThemeContextProvider>
  );
};
