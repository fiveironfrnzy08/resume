module.exports = {
  colors: {
    defaultCompany: '#3f9aaf',
    artifact: '#525299',
    greenhouse: '#3DC4BE',
    flowhub: '#2add93',
    ladder: '#423674'
  },
  text: {
    title: {
      defaultCompany: 'Senior Frontend Engineer',
      artifact: 'Artifact Health - Senior Software Engineer',
      greenhouse: 'Greenhouse - Senior Software Engineer',
      flowhub: 'Flowhub - Senior Fullstack Aficionado',
      ladder: 'Ladder - Senior Frontend Engineer'
    }
  }
};
