import React, { useContext } from 'react';
import MapMarkerIcon from 'mdi-react/MapMarkerIcon';
import CellphoneAndroidIcon from 'mdi-react/CellphoneAndroidIcon';
import LinkedinIcon from 'mdi-react/LinkedinIcon';
import EmailOutlineIcon from 'mdi-react/EmailOutlineIcon';
import { ThemeContext } from '../../ThemeContext';
import { Item } from './Item';
import clsx from 'clsx';

export const Contact = () => {
  const [theme] = useContext(ThemeContext);
  const contentStyles = 'pl-2 mr-6 flex-grow hidden sm:block print:block print:mr-0';
  const iconColor = theme.colors.text.companyPrimary;
  return (
    <div className="flex flex-row w-full sm:flex-col print:flex-col">
      <Item href="tel:612-309-7968" copyText="6123097968">
        <CellphoneAndroidIcon className={iconColor} />
        <div className={clsx(contentStyles)}>612-309-7968</div>
      </Item>
      <Item href="mailto:ryanthomasgray@gmail.com" copyText="ryanthomasgray@gmail.com">
        <EmailOutlineIcon className={iconColor} />
        <div className={clsx('print:hidden', contentStyles)}>Email me</div>
        <div className="hidden print:block print:pl-2">ryanthomasgray@gmail.com</div>
      </Item>
      <Item href="https://linkedin.com/in/ryanthomasgray">
        <LinkedinIcon className={iconColor} />
        <div className={clsx('print:hidden', contentStyles)}>Connect with me</div>
        <div className="hidden print:block print:pl-2">ryanthomasgray</div>
      </Item>
      <Item>
        <MapMarkerIcon className={iconColor} />
        <div className={clsx(contentStyles, 'flex-col')}>
          <div>Denver, CO</div>
        </div>
      </Item>
    </div>
  );
};
