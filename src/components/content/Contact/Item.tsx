import React from 'react';

interface ItemProps {
  href?: string
  copyText?: string
  children: React.ReactNode
}

export const Item = ({href, children}: ItemProps) => (
  <span className="flex content-between flex-1">
    <a
      className="flex flex-col sm:flex-row flex-wrap w-full text-lg print:flex-row"
      href={href}
      target="_blank"
      rel="noopener noreferrer"
    >
      {children}
    </a>
  </span>
);
