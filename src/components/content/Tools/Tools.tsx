import React from 'react';

export const Tools = () => {
  const technologies = [
    'TypeScript',
    'React',
    'GraphQL',
    'Cypress',
    'Jest',
  ];
  return (
    <div className="print:w-full print:justify-center">
      <div className="flex flex-wrap">
        {technologies.map((technology) => {
          return (
            <div
              className="w-full sm:w-full md:w-1/2 text-lg print:w-1/2 print:w-1/2 print:text-sm"
              key={technology}
            >
              {technology}
            </div>
          );
        })}
      </div>
    </div>
  );
};
