import React from 'react';

export const Description = ({ children }: { children: string }) => {
  return <div>{children}</div>;
};
