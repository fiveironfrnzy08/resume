import React, { useContext } from 'react';
import { ThemeContext } from '../../ThemeContext';
import clsx from 'clsx';

export const Header = ({ children }: { children: string }) => {
  const [theme] = useContext(ThemeContext);
  return <div className={clsx('text-lg', theme.colors.text.companyPrimary, 'print:text-sm')}>{children}</div>;
};
