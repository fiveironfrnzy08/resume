import React, { useContext } from 'react';
import { ThemeContext } from '../../ThemeContext';

export const SubHeader = ({ children }: { children: string }) => {
  const [theme] = useContext(ThemeContext);
  return <div className={`${theme.colors.text.companyPrimary}`}>{children}</div>;
};
