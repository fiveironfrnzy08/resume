import * as React from 'react';
import { Header } from './Header';
import { Description } from './Description';
import clsx from 'clsx';

export const Awards = () => {
  const printClasses = 'print:flex print:flex-wrap print:w-1/3 print:flex-col';
  const classes = 'pb-1';
  return (
    <div className="flex-col flex-1 print:flex print:flex-row">
      <div className={clsx(classes, printClasses)}>
        <Header>(2019) Outstanding Service</Header>
        <Description>For extra efforts to advance front-end</Description>
      </div>
      <div className={clsx(classes, printClasses)}>
        <Header>(2019) Peer Award</Header>
        <Description>For being a go-to for mentoring and for front-end improvements</Description>
      </div>
      <div className={clsx(classes, printClasses)}>
        <Header>(2018) Peer Award</Header>
        <Description>For leadership and mentoring of junior devs</Description>
      </div>
    </div>
  );
};
