import React, { PropsWithChildren } from 'react';

interface BaseButtonProps {
  onClick: () => void
}

export const BaseButton = ({ onClick, children }: PropsWithChildren<BaseButtonProps>) => (
  <button
    className={`hidden md:flex justify-center items-center flex-col rounded-full w-20 h-20 bg-white shadow print:hidden`}
    onClick={onClick}
  >
    {children}
  </button>
);
