import React, { useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPrint } from '@fortawesome/free-solid-svg-icons';
import { ThemeContext } from '../../ThemeContext';
import { BaseButton } from './BaseButton';

export const PrintButton = () => {
  const [theme] = useContext(ThemeContext);
  const color = theme.colors.primary;
  const print = () => {
    window.print();
    return false;
  };
  return (
    <BaseButton onClick={print}>
      <FontAwesomeIcon className="mt-2" icon={faPrint} color={color} />
      <div className="text-xs pt-2">
        Print/PDF
      </div>
    </BaseButton>
  );
};
