import React, { useContext } from 'react';
import { ThemeContext } from '../../ThemeContext';

export const SideProjects = () => {
  const [theme] = useContext(ThemeContext);
  const containerClasses = 'flex';
  const yearClasses = `${theme.colors.text.companyPrimary} pr-6`;
  const descriptionClasses = 'text-left';
  return (
    <div>
      <div className={containerClasses}>
        <div className={yearClasses}>(2019)</div>
        <div className={descriptionClasses}>Contracted for creation of a hybrid app in healthcare education (details
          omitted per NDA)
        </div>
      </div>
      <div className={containerClasses}>
        <div className={yearClasses}>(2018)</div>
        <div className={descriptionClasses}>Contracted by American Peddlers to develop FindMyTournament.com. The
          lacrosse tournament search engine was built using a MEAN stack and deployed using CircleCI and AWS Lightsail
        </div>
      </div>
      <div className={containerClasses}>
        <div className={yearClasses}>(2016)</div>
        <div className={descriptionClasses}>Contracted by MN Prematurity Coalition and Children’s Hospital to develop
          hybrid mobile app, PreemiePrep
        </div>
      </div>
      <div className={containerClasses}>
        <div className={yearClasses}>(2015)</div>
        <div className={descriptionClasses}>Contracted by Optum Finance to develop revenue calculator modules in PHP and
          JS for MedSynergies
        </div>
      </div>
      <div className={containerClasses}>
        <div className={yearClasses}>(2014)</div>
        <div className={descriptionClasses}>Wrote Java program to find lowest priced US departure airport. Used to save
          $650/ticket on airfare for honeymoon travel
        </div>
      </div>
      <div className={containerClasses}>
        <div className={yearClasses}>(2014)</div>
        <div className={descriptionClasses}>Wrote web service to control Onkyo receiver over Ethernet home network,
          including power on/off and volume control
        </div>
      </div>
      <div className={containerClasses}>
        <div className={yearClasses}>(2013)</div>
        <div className={descriptionClasses}>Wrote Java program to monitor and record flight history using Expedia.
          Tracked flights for several family members/friends to help determine when to buy
        </div>
      </div>
      <div className={containerClasses}>
        <div className={yearClasses}>(2013)</div>
        <div className={descriptionClasses}>Published Android news aggregator app called Post’d, including Java and
          MySQL backend
        </div>
      </div>
    </div>
  );
};
