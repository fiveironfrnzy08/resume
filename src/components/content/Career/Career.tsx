import React from 'react';
import {Company, Job} from './Company';

export const Career = () => {
  const JS = 'JavaScript';
  const TS = 'TypeScript';
  const REACT = 'React';
  const GQL = 'GraphQL';
  const CYPRESS = 'Cypress';
  const JEST = 'Jest';
  const MUI = 'Material UI';
  const NODE = 'Node.js';
  const ANGULAR = 'AngularJS';
  const CORDOVA = 'Cordova';
  const JAVA = 'Java';
  const SPRING = 'Spring';
  const HIBERNATE = 'Hibernate';
  const KARMA = 'Karma';
  const JASMINE = 'Jasmine';
  const CHAI = 'Chai';
  const MYSQL = 'MySQL';
  const MS_SQL = 'Microsoft SQL';
  const GTM = 'Google Tag Manager';
  const VBA = 'Visual Basic for Applications';
  const MICROS = 'Micros POS';

  const jobs: Job[] = [
    {
      company: 'Zen Planner',
      dates: '2017 - Present',
      positions: [
        {
          title: 'Senior Frontend/Software Engineer',
          descriptions: [
            'Architected frontend for company\'s greenfield start-up, subsequently securing project funding',
            'Lead, empower, and mentor frontend team of 3 engineers'
          ],
          tech: [REACT, TS, MUI, GQL, CYPRESS, JEST]
        },
        {
          title: 'Software Engineer',
          descriptions: [
            'Feature developer for hybrid apps and SaaS platform',
            'Lead front-end advancements, including 176k line change to move to Webpack with no production issues'
          ],
          tech: [JS, CORDOVA, JAVA, MS_SQL, SPRING, HIBERNATE]
        }
      ]
    },
    {
      company: 'Shutterstock',
      dates: '2016 - 2017',
      positions: [
        {
          title: 'Software Engineer',
          descriptions: [
            'Implemented tracking in Google Tag Manager required by marketing stakeholders'
          ],
          tech: [NODE, JS, GTM]
        }
      ]
    },
    {
      company: 'PointClickCare',
      dates: '2013 - 2016',
      positions: [
        {
          title: 'Developer',
          descriptions: [
            'Interim development team lead (Nov 2015 – Apr 2016)',
            'Designed, built, and maintained hybrid mobile app using front end technologies AngularJS and Cordova, and SaaS web app using Java and Spring'
          ],
          tech: [ANGULAR, JS, CORDOVA, NODE, JAVA, MYSQL, SPRING, HIBERNATE, KARMA, JASMINE, CHAI]
        },
        {
          title: 'Account Management Programmer',
          descriptions: [
            'Developed software to process medical claims',
            'Managed daily needs and customer service for 30+ customers including data cleanup, debugging, and bug fixes'
          ],
          tech: [JAVA, MYSQL]
        }
      ]
    },
    {
      company: 'Delaware North Companies',
      dates: '2011 – 2013',
      positions: [
        {
          title: 'IT Administrator',
          descriptions: [
            'Wrote Excel VBA macros to expedite/automate managers’ daily work. Most notable effort saved vending manager 8+hrs of work weekly',
            'Daily IT support for office of 40+ employees'
          ],
          tech: [VBA, MICROS]
        }
      ]
    }
  ];

  return (
    <div className="text-left">
      {jobs.map((job) => {
        return (
          <Company key={job.company} job={job} />
        );
      })}
    </div>
  );
};
