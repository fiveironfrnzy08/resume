import React from 'react';

export type Description = string

interface DescriptionsProps {
  descriptions: Description[]
}

export const Descriptions = ({ descriptions }: DescriptionsProps) => {
  return (
    <React.Fragment>
      {descriptions.map((description) => {
        return <li key={description} className="ml-3">{description}</li>;
      })}
    </React.Fragment>
  );
};
