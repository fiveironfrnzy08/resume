import * as React from 'react';
import { useContext } from 'react';
import { ThemeContext } from '../../ThemeContext';
import { Positions, Position } from './Positions';

export interface Job {
  company: string
  dates: string
  positions: Position[]
}

interface CompanyProps {
  job: Job
}

export const Company = ({ job }: CompanyProps) => {
  const [theme] = useContext(ThemeContext);
  return (
    <div className="pb-3">
      <div className="flex justify-between">
        <span className={`flex text-xl ${theme.colors.text.companyPrimary} print:text-base`}>
          {job.company}
        </span>
        <span className={`flex text-xl ${theme.colors.text.companyPrimary} print:text-base`}>
          {job.dates}
        </span>
      </div>
      <Positions positions={job.positions} />
    </div>
  );
};
