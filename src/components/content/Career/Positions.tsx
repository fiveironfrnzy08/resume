import React, { useContext } from 'react';
import { ThemeContext } from '../../ThemeContext';
import { Description, Descriptions } from './Descriptions';
import clsx from 'clsx';

export interface Position {
  title: string
  tech: string[]
  descriptions: Description[]
}

interface PositionsProps {
  positions: Position[]
}

export const Positions = ({ positions }: PositionsProps) => {
  const [theme] = useContext(ThemeContext);
  return (
    <>
      {positions.map((position, i) => {
        return (
          <div className={clsx('ml-5', i > 0 ? 'mt-2' : '')} key={position.title}>
            <div className={theme.colors.text.companyPrimary}>{position.title}</div>
            <div className="text-gray-600">{position.tech.join(', ')}</div>
            <ul className="ml-1 list-disc">
              <Descriptions descriptions={position.descriptions} />
            </ul>
          </div>
        );
      })}
    </>
  );
};
