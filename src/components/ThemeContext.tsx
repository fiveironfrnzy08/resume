import React, { Dispatch, SetStateAction, useState } from 'react';
import QueryString from 'query-string';
import themeConfig from '../theme.config';

interface Colors {
  primary: string
  text: {
    companyPrimary: string
  }
  border: {
    companyPrimary: string
  }
}

export type Company = 'artifact' | 'flowhub' | 'greenhouse' | 'ladder' | 'defaultCompany'

interface ThemeConfig {
  colors: Record<Company, string>,
  text: {
    title: Record<Company, string>
  }
}

export interface Theme {
  company: Company
  colors: Colors
  text: {
    title: string
  }
}

type ThemeContextType = [Theme, Dispatch<SetStateAction<Theme>>]

interface ThemeContextProviderProps {
  children: React.ReactNode
}

export const ThemeContext = React.createContext<ThemeContextType>([
  {} as Theme,
  (() => {}) as Dispatch<SetStateAction<Theme>>
]);

export const ThemeContextProvider = ({ children }: ThemeContextProviderProps) => {
  const params = QueryString.parse(window.location.search);
  let company: Company = 'defaultCompany';
  if (params && params.company) {
    company = params.company as Company;
  }
  const [theme, setTheme] = useState<Theme>({
    company: company,
    colors: {
      primary: themeConfig.colors[company],
      text: {
        companyPrimary: `text-${company}`
      },
      border: {
        companyPrimary: `border-${company}`
      }
    },
    text: {
      title: (themeConfig as ThemeConfig).text.title[company]
    }
  });
  return (
    <ThemeContext.Provider value={[theme, setTheme]}>
      {children}
    </ThemeContext.Provider>
  );
};

export const config: ThemeConfig = themeConfig;
