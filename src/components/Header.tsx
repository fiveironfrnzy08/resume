import React, { useContext } from 'react';
import me from '../assets/images/ryan-gray.jpg';
import { ThemeContext } from './ThemeContext';

export const Header = () => {
  const [theme] = useContext(ThemeContext);
  return (
    <>
      <header className="text-6xl flex-column justify-center pt-3 print:hidden print:text-3xl">
        <div className="flex flex-col sm:flex-row items-center justify-center">
          <div className="flex flex-row leading-tight">
            <span>Ryan</span>
            <span className="block sm:hidden">
          <span>&nbsp;Gray</span>
        </span>
          </div>
          <img
            className={`rounded-full border-4 border-solid ${theme.colors.border.companyPrimary} mx-3 w-24 h-24`}
            src={me}
            alt="Ryan Gray"
          />
          <div className="hidden sm:block">
            <span>Gray</span>
          </div>
        </div>
        <div className="text-2xl pt-2 text-center print:text-base">
          {theme.text.title}
        </div>
      </header>
      <header className="hidden print:block text-3xl flex-column justify-center pt-3 print:text-xl">
        <div className="flex justify-between px-2">
          <span>Ryan Gray - {theme.text.title}</span>
          <span>ryanthomasgray@gmail.com</span>
        </div>
      </header>
    </>
  );
};
