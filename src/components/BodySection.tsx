import React, { useContext } from 'react';
import { ThemeContext } from './ThemeContext';

interface BodySectionProps {
  title: string
  children: React.ReactNode
  className?: string
}

export const BodySection = ({ title, className, children }: BodySectionProps) => {
  const [theme] = useContext(ThemeContext);
  return (
    <div className={`${className} border-t-4 ${theme.colors.border.companyPrimary} border-solid`}>
      <div className="text-2xl pt-2 print:text-base print:p-1">
        {title}
      </div>
      <div className="flex flex-1 print:p-1">
        {children}
      </div>
    </div>
  );
};
