const { colors } = require('./src/theme.config');

module.exports = {
  theme: {
    screens: {
      sm: '640px',
      md: '768px',
      lg: '1024px',
    },
    colors: {
      white: '#fff',
      paper: '#fafafa',
      primary: '#323232',
      gray: {
        600: '#718096'
      }
    },
    width: theme => ({
      auto: 'auto',
      ...theme('spacing'),
      '1/2': '50%',
      '1/3': '33.333333%',
      '2/3': '66.666667%',
      '1/4': '25%',
      '2/4': '50%',
      '3/4': '75%',
      full: '100%',
      screen: '100vw',
    }),
    boxShadow: {
      default: 'rgba(109, 108, 142, 0.24) 0px 15px 24px -10px',
      heavy: 'rgba(109, 108, 142, 0.24) 0px 15px 56px -10px',
      none: 'none',
    },
    spacing: {
      px: '1px',
      '0': '0',
      '1': '0.25rem',
      '2': '0.5rem',
      '3': '0.75rem',
      '4': '1rem',
      '5': '1.25rem',
      '6': '1.5rem',
      '12': '3rem',
      '16': '4rem',
      '20': '5rem',
      '24': '6rem',
      '32': '8rem',
    },
    cursor: {},
    fontFamily: {},
    fontWeight: {},
    letterSpacing: {},
    lineHeight: {},
    maxWidth: {},
    opacity: {},
    order: {},
    zIndex: {},
    extend: {
      colors: colors,
      borderWidth: {
        '1': '1px',
      },
      container: {
        center: true,
      },
      fontFamily: {
        sans: ['Fira Sans Extra Condensed', 'sans-serif']
      },
      screens: {
        print: {
          raw: 'print'
        }
      }
    },
  }
};
